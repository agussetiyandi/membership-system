<?php

ini_set("display_errors", 1);

// Extend the TCPDF class to create custom Header and Footer
    class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = base_url('assets/images/logo/asd.jpg'); // *** Very IMP: make sure this image is available on given path on your server
        $this->Image($image_file,15,6,0,23,'','','',true);
        // Set font
        $this->SetFont('helvetica', 'RB', 9);
    
        // Line break
        $this->Ln(5);        
        $this->Cell(0, 0, "PT. Medianusa Permana", 0, false, 'R', 0, '', 1, false, 'M', 'M');
        $this->SetFont('helvetica', 'R', 9);
        $this->Ln(5);        
        $this->Cell(0, 0, "Komp. Perkantoran Graha Kadin Blok F No. 5, Batam Center", 0, false, 'R', 0, '', 0, false, 'M', 'M');
        $this->Ln(5);        
        $this->Cell(0, 0, "Batam - Kepulauan Riau – Indonesia", 0, false, 'R', 0, '', 0, false, 'M', 'M');
        $this->Ln(5);        
        $this->Cell(0, 0, "Ph. +62 (778) 468817 / 18 / 19; Fax. +62 (778) 460787", 0, false, 'R', 0, '', 0, false, 'M', 'M');
        // We need to adjust the x and y positions of this text ... first two parameters
        
    }

    // Page footer
    public function Footer() {
        /*// Position at 25 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        
        $this->Cell(0, 0, 'Product Company - ABC test company, Phone : +91 1122 3344 55, TIC : TESTTEST', 0, 0, 'C');
        $this->Ln();
        $this->Cell(0,0,'www.clientsite.com - T : +91 1 123 45 64 - E : info@clientsite.com', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');*/
    }
    
}

///////////////// End of class ////////////////////////////

//////////////////////////////////
//
//
// Create new PDF document
//
//
//////////////////////////////////

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nilesh Zemse');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('Invoice');
$pdf->SetKeywords('PDF, Invoice');


// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
// $pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10); 
// *** Very IMP: Please use times font, so that if you send this pdf file in gmail as attachment and if user
//opens it in google document, then all the text within the pdf would be visible properly.

// add a page
$pdf->AddPage();
$no = 1;
// print_r($data);
// die();
// create some HTML content

$u_nama = $data["u_nama"];
$u_level = $data["u_level"];
$u_position = $data["u_position"];

$date = date("d F Y", strtotime("now"));

foreach($purchase_order->result() as $quote) :
    $po_date = $quote->po_date;
    $po_number = $quote->no_purchase_order;
    $nama_perusahaan = $quote->nama_perusahaan;
    $email = $quote->email;
    $nama_pic = $quote->nama_pic;
endforeach;


// *** IMP: The value of $html and $html_terms can come from db
// But, If these values contain, other language special characters, then
// PDF is not getting generated. in that case should find such invalid charactes and 
// make use of its htmlentity substitute 
// for ex. If copyright is invalid character then use &copy; in html content


// $html on page 1 of PDF and $html_terms are on page 2 of PDF


$html = '<br><br>';
$html .= '<table style="font-size: 13px">
            <tr>
                <td colspan="6" align="right"><h1>Purchase Order</h1></td>
            </tr>
            <tr>
                <td>PO Date</td>
                <td colspan="3">: {po_date}</td>
            </tr>
            <tr>
                <td>PO Number</td>
                <td colspan="3">: {po_number}</td>
            </tr>
            <tr>
                <td>Perusahaan</td>
                <td colspan="3">: {nama_perusahaan}</td>
            </tr>
            <tr>
                <td>Mail to</td>
                <td colspan="3">: {email}</td>
            </tr>
         </table>';

//pembuka
// $html .= '
//           <p style="text-align: justify;">Dengan Hormat,<br><br><b>PT. Medianusa Permana</b> adalah sebuah perusahaan yang bergerak di bidang IT dengan layanan Konsultasi IT, Multimedia dan Penyedia Layanan Internet. Perusahaan kami sudah berperpengalaman lebih dari 7 Tahun menyediakan <b>“One Stop Completely Solution”</b>, untuk kebutuhan telekomunikasi data di Perusahaan Anda. Berikut kami sampaikan penawaran dari kami </p>
//         ';

//pembuka
$html .= '
        <table border="1" cellpadding="5">
            <tr style="background-color: #C5C5C6">
                <td align="center" width="5%"><b>No</b></td>
                <td align="center" width="30%"><b>Keterangan</b></td>
                <td align="center"><b>Unit</b></td>
                <td align="center"><b>Harga Satuan</b></td>
                <td align="center"><b>Total</b></td>
            </tr>';
$no = 1;
foreach($purchase_order_item->result() as $row) :
$html .= '<tr>
                <td align="center">'.$no.'</td>
                <td align="center">'.$row->produk.'<br>'.$row->media_akses.'<br>'.$row->alamat_instalasi.'<br>'.$row->alamat_backhaule.'</td>
                <td align="center">'.$row->kapasitas.'</td>
                <td align="center">Rp'.number_format($row->harga_satuan).'</td>
                <td align="center">Rp'.number_format($row->total).'</td>
            </tr>';
$no++;
endforeach;
if($no == 1):
    $html .= '<tr>
            <td align="center" colspan="7">No Data</td>
        </tr>';
endif;
$html .= '</table>
        <table>
            <tr><td></td></tr>
        </table>
        ';

//syarat & penutup
$html .= '<br>
          <b>Syarat dan ketentuan</b>
          <ul style="padding: 0px;">
            <li>Penawaran berlaku 14  hari sejak tanggal penawaran</li>
            <li>SLA 99.5%</li>
            <li>Free Router Mikrotik RB941</li>
            <li>Helpdesk   7 X 24 jam</li>
            <li>Pemutusan kontrak sebelum waktu kontrak selesai secara sepihak oleh pelanggan akan dikenakan denda sebesar 50% x biaya bulanan x sisa masa berlangganan.</li>
            <li>Berlanggan tidak sampai 1 tahun, pembayaran dilakukan diawal ( biaya instalasi dan berlangganan paket selama periode tertentu )</li>
            <li>Lama Pengerjaan – Wireless 7 hari maksimal setelah terima PO</li>
          </ul>
          <p>
            Untuk informasi lebih lanjut, Mohon untuk bisa menghubungi kami melalui email atau telephone. 
            Atas perhatian dan kerjasama yang baik, Kami ucapkan terima kasih.
          </p>
        ';

//ttd
$html.='
        <table style="font-size: 14px">
            <tr>
                <td align="center">Hormat kami,</td>
                <td></td>
                <td align="center">Persetujuan</td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr>
                <td align="center">{u_nama}</td>
                <td></td>
                
            </tr>
            <tr>
                <td align="center" style="border-top: 1px solid black;">{u_position}</td>
                <td></td>
                <td style="border-top: 1px solid black"></td>
            </tr>
        </table>
        ';


$html = str_replace('{email}',$email, $html);
$html = str_replace('{perusahaan}',$perusahaan, $html);
$html = str_replace('{nama_pelanggan}',$nama, $html);
$html = str_replace('{date}',$date, $html);
$html = str_replace('{no_surat}',$no_surat, $html);
$html = str_replace('{u_nama}',$u_nama, $html);
$html = str_replace('{u_position}',$u_position, $html);


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// add a page
//Close and output PDF document
$pdf_file_name = 'Quotation.pdf';
$pdf->Output($pdf_file_name, 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
?>