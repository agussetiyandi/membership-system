<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->library('Pdf');
		$this->load->model("Company_mdl");
	}

	// ADD COMPANY
	public function index(){
		$this->load->view('addCompany');
	}

	public function addCompany(){
		$this->load->view('addCompany');
	}

	public function addCompanyCreateProcess(){
		$config['upload_path']    = './assets/images/company/';
		$config['allowed_types']  = 'gif|jpg|png';
		$config['overwrite']			= true;
		$config['overwrite']			= true;

		$this->load->library('upload',$config);
		$this->upload->do_upload('picture');

		$upload_data = $this->upload->data(); 
		$file_name =   $upload_data['file_name'];
			
		$data = array(
			'company_name' => $this->input->post('company_name'),
			'company_email' => $this->input->post('company_email'),
			'company_phone' => $this->input->post('company_phone'),
			'company_category' => $this->input->post('company_category'),
			'company_qty_employee' => $this->input->post('company_qty_employee'),
			'company_desc' => $this->input->post('company_desc'),
			'company_pic' => $this->input->post('company_pic'),
			'created_by' => $this->session->userdata("id"),
			'company_logo' => $file_name
		);
		$this->Company_mdl->companyCreateProcessDB($data);
		redirect('Company/companyList');
	}

	// lIST COMPANY
	public function companyList(){
		$data['t_company'] = $this->Company_mdl->get_all_data_company();
		$this->load->view('companyList', $data);
	}

	// UPDATE COMPANY
	public function companyUpdate($company_id){
		$data['t_company'] = $this->Company_mdl->get_data_company($company_id);
		$this->load->view('companyUpdate', $data);
	}

	public function companyUpdateProcess(){
		$config['upload_path']    = './assets/images/company/';
		$config['allowed_types']  = 'gif|jpg|png';
		$config['overwrite']			= true;
		$config['overwrite']			= true;

		$this->load->library('upload',$config);
		$this->upload->do_upload('picture');

		$upload_data = $this->upload->data(); 
  	$file_name =   $upload_data['file_name'];

		$data = array(
			'company_name' => $this->input->post('company_name'),
			'company_email' => $this->input->post('company_email'),
			'company_phone' => $this->input->post('company_phone'),
			'company_category' => $this->input->post('company_category'),
			'company_qty_employee' => $this->input->post('company_qty_employee'),
			'company_desc' => $this->input->post('company_desc'),
			'company_pic' => $this->input->post('company_pic'),
			'company_logo' => $file_name
		);
		$condition['company_id'] = $this->input->post('company_id');
		$this->Company_mdl->companyUpdateProcessDB($data, $condition);
		redirect('Company/companyList');
	}

	// DELETE COMPANY
	public function companyDeleteProses($company_id){
		$this->Company_mdl->companyDeleteProsesDB($company_id);
		redirect('Company/companyList');
	}

	
	// public function quotationDetail($id){
	// 	$data['penawaran'] = $this->Quotation_model->get_all_data_quotation_detail($id);
	// 	$data['syarat_ketentuan'] = $this->Quotation_model->get_syarat_ketentuan($id)->num_rows();
	// 	$data['id_quotation'] = $id;
	// 	$this->load->view('quotationDetail', $data);
	// }

	// public function quotationDetailCreate($id_quotation){
	// 	$data['action'] = "Create";
	// 	$data['id_quotation'] = $id_quotation;
	// 	$this->load->view('modal', $data);
	// }

	// public function quotationDetailCreateProcess(){
	// 	$data = array(
	// 		'id_quotation' => $this->input->post('id_quotation'),
	// 		'produk' => $this->input->post('produk'),
	// 		'segmen' => $this->input->post('segmen'),
	// 		'kapasitas' => $this->input->post('kapasitas'),
	// 		'media_akses' => $this->input->post('media_akses'),
	// 		'kontrak' => $this->input->post('kontrak'),
	// 		'alamat_instalasi' => $this->input->post('alamat_instalasi'),
	// 		'biaya_instalasi' => $this->input->post('biaya_instalasi'),
	// 		'biaya_bulanan' => $this->input->post('biaya_bulanan')
	// 	);
	// 	$id_quotation = $this->input->post('id_quotation');
	// 	$this->Quotation_model->quotationDetailCreateProcessDB($data);
	// 	redirect('Quotation/quotationDetail/'.$id_quotation);
	// }

	// public function quotationDetailDeleteProses($id, $id_quotation){
	// 	$this->Quotation_model->quotationDetailDeleteProsesDB($id);
	// 	redirect('Quotation/quotationDetail/'.$id_quotation);
	// }

	// public function quotationSKCreate($id_quotation){
	// 	$data['action'] = "Create";
	// 	$data['id_quotation'] = $id_quotation;
	// 	$this->load->view('modalSK', $data);
	// }

	// public function quotationSKCreateProcess(){
	// 	$post=$this->input->post();
	// 	$isi_sk = (isset($post->sk)?array_keys($post->sk):array());
	// 	$data = array(
	// 		'id_quotation' => $this->input->post('id_quotation'),
	// 		'isi_sk' => $this->input->post('sk')
	// 	);
	// 	$id_quotation = $this->input->post('id_quotation');
	// 	$this->Quotation_model->quotationSKCreateProcessDB($data);
	// 	redirect('Quotation/quotationDetail/'.$id_quotation);
	// }
}
