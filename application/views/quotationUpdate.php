<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Purchase Order System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Dashboard</h4>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<?php foreach($quotation->result() as $row) : ?>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h6>Data Customer</h6>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-md-8">
										<form method="POST" action="<?php echo site_url('Quotation/quotationUpdateProcess') ?>">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Nama Perusahaan</label>
												<div class="col-sm">
													<input name="perusahaan" type="text" class="form-control" placeholder=". . ." value="<?= $row->perusahaan ?>" required>
													<input name="id" type="hidden" class="form-control" placeholder=". . ." value="<?= $row->id ?>" required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Nama PIC</label>
												<div class="col-sm">
													<input name="nama" type="text" class="form-control" placeholder=". . ." value="<?= $row->nama ?>" required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Jabatan</label>
												<div class="col-sm">
													<input name="jabatan" type="text" class="form-control" placeholder=". . ." value="<?= $row->jabatan ?>" required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">E-mail</label>
												<div class="col-sm">
													<input name="email" type="mail" class="form-control" placeholder=". . ." value="<?= $row->mail ?>" required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Kategori Segmen</label>
												<div class="col-sm">
													<select name="category" class="form-control" required>
														<option value="Broadband" <?php echo (($row->category == "Broadband") ? "selected" : ""); ?>>Broadband</option>
														<option value="Dedicated" <?php echo (($row->category == "Dedicated") ? "selected" : ""); ?>>Dedicated</option>
														<option value="Hospitality" <?php echo (($row->category == "Hospitality") ? "selected" : ""); ?>>Hospitality</option>
														<option value="Solution" <?php echo (($row->category == "Solution") ? "selected" : ""); ?>>Solution </option>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2"></label>
												<div class="col-sm">
													<button type="submit" class="btn btn-success pull-right" style="margin: 0px 5px;"><i class="fa fa-save"></i> Save</button>
													<a href="<?php echo site_url('Quotation/quotationList') ?>" class="btn btn-danger pull-right" style="margin: 0px 5px;"><i class="fa fa-times"></i> Cancel</a>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<?php endforeach; ?>
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->
	<div id="modalCE" class="modal fade">
		<div class="modal-dialog" id="modalContent">

		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>

	<script type="text/javascript">
		function modalCreate() {
			$.ajax({
					url: "<?php echo site_url('Printer/modalCreate') ?>",
					success: function(data){
						$("#modalContent").html(data);
					}
				});
			$("#modalCE").modal("show");
		}
	</script>
</body>

</html>
