<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("User_model"); 
	}

	public function index(){
		$this->load->view('addUser');
	}

	public function addUser(){
		$this->load->view('addUser');
	}

	public function addUserCreateProcess(){
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => MD5($this->input->post('password')),
			'level' => $this->input->post('level_akses'),
			'position' => $this->input->post('position')
		);
		$this->User_model->userCreateProcessDB($data);
		redirect('User/userList');
	}
	
	function userList(){
		$data['user'] = $this->User_model->get_all_data_user();
		$this->load->view('userList', $data);
	}

	function userUpdate($id){
		$data['user'] = $this->User_model->get_data_user($id);
		$this->load->view('userUpdate', $data);
	}

	function userUpdateProcess(){
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => MD5($this->input->post('password')),
			'position' => $this->input->post('position'),
			'level' => $this->input->post('level')
		);
		if($this->input->post('password') == ""){
			unset($data['password']);
		}
		$condition['id'] = $this->input->post('id');
		$this->User_model->userUpdateProcessDB($data, $condition);
		redirect('User/userList');
	}

	function userDeleteProses($id){
		$this->User_model->userDeleteProsesDB($id);
		redirect('User/userList');
	}
}
