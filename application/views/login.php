<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Login</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="author">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico"><!-- Basic Css files -->
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css">
</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<div class="accountbg"></div>
	<div class="wrapper-page">
		<div class="card">
			<div class="card-body">
				<div class="p-3">
					<div class="float-right text-right">
						<h4 class="font-18 mt-3 m-b-5">Welcome Back !</h4>
						<p class="text-muted">Sign in to Membership System</p>
					</div><a href="index.html" class="logo-admin"><img src="#" height="50" alt="logo"></a>
				</div>
				<div class="p-3">
					<form class="form-horizontal m-t-10" action="<?php echo base_url('login/aksi_login'); ?>" method="post">
						<?php if($this->session->flashdata('msg') != NULL){?>
						<div class="alert alert-danger">
							<?php echo $this->session->flashdata('msg');?>
						</div>
						<?php } ?>
						<div class="form-group"><label for="username">Username / Email</label> <input type="text"
								class="form-control" name="username" id="username" placeholder="Enter username" required
								autocomplete="off"></div>
						<div class="form-group"><label for="password">Password</label> <input type="password" class="form-control"
								name="password" id="password" placeholder="Enter password" required></div>
						<div class="form-group row m-t-30">

							<div class="col-sm-12 text-right"><button name="submit"
									class="btn btn-primary w-md waves-effect waves-light" type="submit">Log
									In</button></div>
						</div>
						<!-- <div class="form-group m-t-30 mb-0 row">
							<div class="col-12 text-center"><a href=" " class="text-muted"><i class="mdi mdi-lock"></i>
									Forgot your password?</a></div>
						</div> -->
					</form>
				</div>
			</div>
		</div>
		<div class="m-t-40 text-center text-white-50">
			<p>&#169; <script type='text/javascript'>
					var creditsyear = new Date();
					document.write(creditsyear.getFullYear());
				</script> <a expr:href='data:blog.homepageUrl'>
					<data:blog.title /></a> Kingstech Pte. Ltd. All Rights Reserved.</p>
		</div>
	</div><!-- end wrapper-page -->
	<!-- jQuery  -->
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/modernizr.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
	<script src="<?php echo base_url();?>assets/js/waves.js"></script><!-- App js -->
	<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>

</html>
