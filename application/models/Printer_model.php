<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printer_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	// PENAWARAN QUOTATION
	function get_all_data_penawaran()
	{
		$this->db->select("*");
		$this->db->from("penawaran");

		return $this->db->get();
	}

	function get_data_penawaran($data)
	{
		$this->db->select("*");
		$this->db->from("penawaran");
		$this->db->where("id_quotation", $data);

		return $this->db->get();
	}

	function get_draft($data)
	{
		$this->db->select("*");
		$this->db->from("quotation");
		$this->db->where("id", $data);

		$draft_result = $this->db->get();
		foreach($draft_result->result() as $quote) :
		    $draft = $quote->category;
		endforeach;
		return $draft;
	}

	// PURCHASE ORDER ITEM
	function get_all_data_purchase_order_item()
	{
		$this->db->select("*");
		$this->db->from("purchase_order_item");

		return $this->db->get();
	}

	function get_data_purchase_order_item($data)
	{
		$this->db->select("*");
		$this->db->from("purchase_order_item");
		$this->db->where("id_purchase_order", $data);

		return $this->db->get();
	}

}
