<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Purchase Order System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">User Management</h4>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h6>Add User</h6>
							</div>
							<?php foreach($user->result() as $row) : ?>
							<div class="card-body">
								<div class="row">
									<div class="col-md-8">
										<form method="POST" action="<?php echo site_url('User/userUpdateProcess') ?>">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Nama</label>
												<div class="col-sm">
													<input name="id" type="hidden" value="<?= $row->id ?>" required>
													<input name="name" type="text" class="form-control" placeholder=". . ." value="<?= $row->name ?>" required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Username</label>
												<div class="col-sm">
													<input name="username" type="text" class="form-control" placeholder=". . ." value="<?= $row->username ?>" required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Password</label>
												<div class="col-sm">
													<input name="password" type="password" class="form-control" placeholder="Type for New Password">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Jabatan</label>
												<div class="col-sm">
													<input name="position" type="text" class="form-control" placeholder=". . ." value="<?= $row->position ?>" required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Level Akses</label>
												<div class="col-sm">
													<select name="level" class="form-control" required>
														<option <?php echo($row->level == "Sales" ? "selected" : ""); ?> value="Sales">Account Manager</option>
														<option <?php echo($row->level == "Manager" ? "selected" : ""); ?> value="Manager">Manager</option>
														<option <?php echo($row->level == "Purchasing" ? "selected" : ""); ?> value="Purchasing">Purchasing</option>
														<option <?php echo($row->level == "Admin" ? "selected" : ""); ?> value="Admin">Administrator</option>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2"></label>
												<div class="col-sm">
													<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div><!-- end row -->
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->
	<div id="modalCE" class="modal fade">
		<div class="modal-dialog" id="modalContent">
			
		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>

	<script type="text/javascript">
		function modalCreate() {
			$.ajax({
					url: "<?php echo site_url('Printer/modalCreate') ?>",
					success: function(data){
						$("#modalContent").html(data);
					}
				});
			$("#modalCE").modal("show");
		}
	</script>
</body>

</html>
