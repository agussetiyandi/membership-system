<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Purchase Order System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">User Management</h4>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h6 class="pull-left">User List</h6>
							</div>
							<div class="card-body">
								<table id="datatable" class="table table-bordered dt-responsive text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="1%">No</th>
											<th>Name</th>
											<th>Username</th>
											<th>Jabatan</th>
											<th>Level</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach($user->result() as $row) : ?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?= $row->name; ?></td>
											<td><?= $row->username; ?></td>
											<td><?= $row->position; ?></td>
											<td><?= $row->level; ?></td>
											<td>
												<div class="btn-group" style="color:white; border-radius: 5px; overflow: hidden;">
													<a href="<?php echo site_url('User/userUpdate/'.$row->id) ?>" title="Edit" type="button" class="btn btn-warning">
														<i class="fa fa-pencil"></i>
													</a>
													<?php if($this->session->userdata("level") == "Admin" ): ?>
													<a href="<?php echo site_url('User/userDeleteProses/'.$row->id) ?>" onclick="return confirm('Are you sure?')" title="Delete" type="button" class="btn btn-danger">
														<i class="fa fa-times"></i>
													</a>
													<?php endif; ?>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->
	<div id="modalCE" class="modal fade">
		<div class="modal-dialog" id="modalContent">
			
		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>

	<script type="text/javascript">
		$("#datatable").DataTable();
		function modalCreate() {
			$.ajax({
					url: "<?php echo site_url('Printer/modalCreate') ?>",
					success: function(data){
						$("#modalContent").html(data);
					}
				});
			$("#modalCE").modal("show");
		}
	</script>
</body>

</html>
