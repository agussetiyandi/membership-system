<?php $level = $this->session->userdata("level"); ?>
<div id="wrapper">
	<!-- Top Bar Start -->
	<div class="topbar">
		<!-- LOGO -->
		<div class="topbar-left">
			<a href="#" class="logo"><img src="#" alt="" height="30"
				 class=" logo-large"="logo-large">
				<img src="<?php echo base_url();?>assets/images/logo/icon_mp.png" alt="" height="22" class="logo-sm"></a>
		</div>
		<nav class="navbar-custom">
			<!-- Search input -->
			<div class="search-wrap" id="search-wrap">
				<div class="search-bar"><input class="search-input" type="search" placeholder="Search">
					<a href="#" class="close-search toggle-search" data-target="#search-wrap">
						<i class="mdi mdi-close-circle"></i>
					</a>
				</div>
			</div>
			<ul class="navbar-right d-flex list-inline float-right mb-0">
				<!-- <li class="list-inline-item dropdown notification-list flags-dropdown d-none d-sm-inline-block">
					<a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button"
					 aria-haspopup="false" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/flags/us_flag.jpg"
						 alt="" class="flag-img">
						English
						<i class="mdi mdi-chevron-down"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-animated">
						<a href="#" class="dropdown-item"><img src="<?php echo base_url();?>assets/images/flags/indonesia_flag.png" alt=""
							 class="flag-img">
							Indonesia</a>
				</li> -->

				<!-- User-->
				<li class="list-inline-item dropdown notification-list">
					<a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
					 aria-haspopup="false" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/users/avatar-8.jpg"
						 alt="user" class="rounded-circle">
						<span class="d-none d-md-inline-block ml-1">
							<?php echo $this->session->userdata("nama"); ?>
							<i class="mdi mdi-chevron-down"></i></span></a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
						<a class="dropdown-item" href="<?php echo base_url('profile'); ?>">
							<i class="dripicons-user text-muted"></i>
							Profile</a>
						<!-- <a class="dropdown-item" href="#">
							<i class="dripicons-gear text-muted"></i>
							Settings</a> -->
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="<?php echo base_url('login/logout'); ?>">
							<i class="dripicons-exit"></i>
							Logout</a>
					</div>
				</li>
			</ul>
			<ul class="list-inline menu-left mb-0">
				<li class="float-left">
					<button class="button-menu-mobile open-left waves-effect">
						<i class="mdi mdi-menu"></i>
					</button>
				</li>
			</ul>
		</nav>
	</div>
	<!-- Top Bar End -->
	<!-- ========== Left Sidebar Start ========== -->
	<div class="left side-menu">
		<div class="slimscroll-menu" id="remove-scroll">
			<!--- Sidemenu -->
			<div id="sidebar-menu">
				<!-- Left Menu Start -->
				<ul class="metismenu" id="side-menu">
					<li class="menu-title">Main</li>
					<li>
						<a href="<?php echo site_url('Dashboard') ?>" class="waves-effect">
							<i class="dripicons-meter"></i>
							<span>Dashboard</span>
						</a>
					</li>

					<?php
						if($level == 'Admin'){
					?>
					
					<!-- QUOTATION -->
					<!-- <li>
						<a href="javascript:void(0);" class="waves-effect">
							<i class="dripicons-view-list"></i>
							<span>
								Quotation
								<span class="float-right menu-arrow">
									<i class="mdi mdi-chevron-right"></i></span></span></a>
						<ul class="submenu">
							<li>
								<a href="<?php echo site_url('Quotation/quotationList') ?>">List Quotation</a>
							</li>
							<li>
								<a href="<?php echo site_url('Quotation/quotationCreate') ?>">Create Quotation</a>
							</li>
						</ul>
					</li> -->

					<!-- PURCHASE ORDER -->
					<!-- <li>
						<a href="javascript:void(0);" class="waves-effect">
							<i class="dripicons-view-list"></i>
							<span>
								Purchase Order
								<span class="float-right menu-arrow">
									<i class="mdi mdi-chevron-right"></i></span></span></a>
						<ul class="submenu">
							<li>
								<a href="<?php echo site_url('PO/purchaseOrderList') ?>">List Purchase Order</a>
							</li>
							<li>
								<a href="<?php echo site_url('PO/purchaseOrderCreate') ?>">Create Purchase Order</a>
							</li>
						</ul>
					</li> -->

					<!-- COMPANY -->
					<li>
						<a href="javascript:void(0);" class="waves-effect">
							<i class="dripicons-view-list"></i>
							<span>
								Company
								<span class="float-right menu-arrow">
									<i class="mdi mdi-chevron-right"></i></span></span></a>
						<ul class="submenu">
							<li>
								<a href="<?php echo site_url('Company/companyList') ?>">Data Company</a>
							</li>
							<li>
								<a href="<?php echo site_url('Company/addCompany') ?>">Register Company</a>
							</li>
						</ul>
					</li>

					<!-- STAFF -->
					<li>
						<a href="javascript:void(0);" class="waves-effect">
							<i class="dripicons-view-list"></i>
							<span>
								Staff
								<span class="float-right menu-arrow">
									<i class="mdi mdi-chevron-right"></i></span></span></a>
						<ul class="submenu">
							<li>
								<a href="<?php echo site_url('Staff/staffList') ?>">List Staff</a>
							</li>
							<li>
								<a href="<?php echo site_url('Staff/companyCreate') ?>">Register Staff</a>
							</li>
						</ul>
					</li>

					<!-- USER MANAGEMENT -->
					<li>
						<a href="javascript:void(0);" class="waves-effect">
							<i class="dripicons-view-list"></i>
							<span>
								User Management
								<span class="float-right menu-arrow">
									<i class="mdi mdi-chevron-right"></i></span></span></a>
						<ul class="submenu">
							<li>
								<a href="<?php echo site_url('User/userList') ?>">List User</a>
							</li>
							<li>
								<a href="<?php echo site_url('User/addUser') ?>">Create User</a>
							</li>
						</ul>
					</li>
					<?php
						}else if($level == 'Sales'){
					?>
					<!-- ADD QUOTATION -->
					<li>
						<a href="javascript:void(0);" class="waves-effect">
							<i class="dripicons-view-list"></i>
							<span>
								Quotation
								<span class="float-right menu-arrow">
									<i class="mdi mdi-chevron-right"></i></span></span></a>
						<ul class="submenu">
							<li>
								<a href="<?php echo site_url('Quotation/quotationList') ?>">List Quotation</a>
							</li>
							<li>
								<a href="<?php echo site_url('Quotation/quotationCreate') ?>">Create Quotation</a>
							</li>
						</ul>
					</li>
					<?php
				}else if($level == 'Manager'){
					?>
					<!-- ADD QUOTATION -->
					<li>
						<a href="javascript:void(0);" class="waves-effect">
							<i class="dripicons-view-list"></i>
							<span>
								Quotation
								<span class="float-right menu-arrow">
									<i class="mdi mdi-chevron-right"></i></span></span></a>
						<ul class="submenu">
							<li>
								<a href="<?php echo site_url('Quotation/quotationList') ?>">List Quotation</a>
							</li>
							<li>
								<a href="<?php echo site_url('Quotation/quotationCreate') ?>">Create Quotation</a>
							</li>
						</ul>
					</li>
					<?php
						}else if($level == 'Purchasing'){
					?>
					<!-- ADD PURCHASE ORDER -->
					<li>
						<a href="javascript:void(0);" class="waves-effect">
							<i class="dripicons-view-list"></i>
							<span>
								Purchase Order
								<span class="float-right menu-arrow">
									<i class="mdi mdi-chevron-right"></i></span></span></a>
						<ul class="submenu">
							<li>
								<a href="<?php echo site_url('PO/purchaseOrderList') ?>">List Purchase Order</a>
							</li>
							<li>
								<a href="<?php echo site_url('PO/purchaseOrderCreate') ?>">Create Purchase Order</a>
							</li>
						</ul>
					</li>
					<?php
						}else{
							echo $this->session->set_flashdata('msg','Login First');
							redirect(base_url('login'));
						}
					?>
				</ul>
				<ul class="metismenu" id="side-menu">
					<li class="menu-title">Helpdesk</li>
				</ul>
			</div>
			<!-- Sidebar -->
			<div class="clearfix"></div>
		</div>
		<!-- Sidebar -left -->
	</div>
	<!-- Left Sidebar End -->
