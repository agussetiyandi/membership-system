<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Purchase Order System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Dashboard</h4>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h6 class="pull-left">Data Purchase Order</h6>
								<button onClick="modalCreate();" type="button" class="btn btn-success pull-right">
									<i class="fa fa-plus"> Add Item</i>
								</button>
								<a href="<?php echo site_url('Printer/printpurchaseorder/'.$id_purchase_order) ?>" target="_blank" class="btn btn-success pull-right mx-1">
									<i class="fa fa-print"> Print</i>
								</a>
							</div>
							<div class="card-body">
								<table id="datatable" class="table table-bordered dt-responsive text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="1%">No</th>
											<th>Produk/Layanan</th>
											<th>Media Akses</th>
											<th>Alamat Instalasi</th>
											<th>Alamat Backhaule</th>
											<th>Kapasitas</th>
											<th>Harga Satuan</th>
											<th>Total</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach($purchase_order_item->result() as $row) : ?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?= $row->produk; ?></td>
											<td><?= $row->media_akses; ?></td>
											<td><?= $row->alamat_instalasi; ?></td>
											<td><?= $row->alamat_backhaule; ?></td>
											<td><?= $row->kapasitas; ?></td>
											<td>Rp<?= number_format($row->harga_satuan) ?></td>
											<td>Rp<?= number_format($row->total) ?></td>
											<td>
												<a href="<?php echo site_url('PO/purchaseOrderDetailDeleteProses/'.$row->id.'/'.$id_purchase_order) ?>" onclick="return confirm('Are you sure?')" title="Delete" type="button" class="btn btn-danger">
													<i class="fa fa-times"></i>
												</a>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->
	<div id="modalCE" class="modal fade">
		<div class="modal-dialog" id="modalContent">
			
		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>

	<script type="text/javascript">
		$("#datatable").DataTable();
		function modalCreate() {
			$.ajax({
					url: "<?php echo site_url('PO/purchaseOrderDetailCreate/'.$id_purchase_order) ?>",
					success: function(data){
						$("#modalContent").html(data);
					}
				});
			$("#modalCE").modal("show");
		}
	</script>
</body>

</html>
