-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2020 at 02:24 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kgsdb_membership_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `penawaran`
--

CREATE TABLE `penawaran` (
  `id` int(11) NOT NULL,
  `id_quotation` int(11) NOT NULL,
  `produk` varchar(255) NOT NULL,
  `segmen` varchar(255) DEFAULT NULL,
  `kapasitas` varchar(255) NOT NULL,
  `media_akses` varchar(255) NOT NULL,
  `kontrak` varchar(255) NOT NULL,
  `alamat_instalasi` varchar(255) NOT NULL,
  `biaya_instalasi` varchar(255) NOT NULL,
  `biaya_bulanan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penawaran`
--

INSERT INTO `penawaran` (`id`, `id_quotation`, `produk`, `segmen`, `kapasitas`, `media_akses`, `kontrak`, `alamat_instalasi`, `biaya_instalasi`, `biaya_bulanan`) VALUES
(4, 3, 'Mouse', 'Hardware', '', 'Kabel', '1 tahun', '', '1 juta', '2oo rb'),
(5, 6, 'Permana Home/Office StartUp', NULL, '', 'Wireless', '1 Tahun', '', '1500000', '500000'),
(7, 7, 'Permana Solution', 'Metro E JF / Metro E Standar', '50 Mbps', 'Fiber Optik', '1 Tahun', 'Komp. Nagoya Point Blok D #11', '1500000', '1000000'),
(8, 8, 'Permana Home/Startup', 'Broadband', '5 Mbps', 'Wireless', '1 tahun', 'Batam', '900000', '500000'),
(9, 10, 'Permana Enterprise', 'Dedicated', '10 Mbps', 'Fiber Optik', '1 Tahun', 'Jl. Merak', '5000000', '4500000'),
(10, 11, 'Permana Hospitality', 'Hospitality', '30 Mbps', 'Fiber Optik + Backup Wireless', '1 Tahun', 'Hotel ABCD', '2500000', '8000000'),
(11, 11, 'Permana Hospitality', 'Hospitality', '50 Mbps', 'Fiber Optic Backup Wireless', '1 Tahun', 'Hotel ABCD', '2500000', '1400000'),
(12, 11, 'Produk Web Hosting dan Cloud', 'Cloud', '500 Gb', 'Cloud', '1 Tahun', 'AAA', '0', '3000000');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `no_purchase_order` varchar(255) DEFAULT NULL,
  `nama_perusahaan` varchar(255) NOT NULL,
  `nama_pic` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `create_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `no_purchase_order`, `nama_perusahaan`, `nama_pic`, `alamat`, `note`, `email`, `date`, `create_by`) VALUES
(1, 'PO-14/MP/III/2019', 'PT Medianusa Permana', 'Agus', 'Graha Kadin', 'Tidak Ada', 'agus@permana.net.id', '2019-03-06 07:28:07', 0),
(5, NULL, 'PT Sehat', 'kdnksdk', 'jdosjo', 'mdls', 'jdsj', '2019-03-12 08:20:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_item`
--

CREATE TABLE `purchase_order_item` (
  `id` int(11) NOT NULL,
  `id_purchase_order` varchar(255) NOT NULL,
  `produk` varchar(255) NOT NULL,
  `media_akses` varchar(255) NOT NULL,
  `alamat_instalasi` varchar(255) NOT NULL,
  `alamat_backhaule` varchar(255) NOT NULL,
  `kapasitas` varchar(255) NOT NULL,
  `harga_satuan` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order_item`
--

INSERT INTO `purchase_order_item` (`id`, `id_purchase_order`, `produk`, `media_akses`, `alamat_instalasi`, `alamat_backhaule`, `kapasitas`, `harga_satuan`, `total`) VALUES
(1, '1', 'Testing', 'Testing', 'Testing', 'Testing', 'Testing', '1000', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` int(11) NOT NULL,
  `no_quotation` varchar(200) DEFAULT NULL,
  `category` varchar(200) NOT NULL,
  `perusahaan` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `mail` varchar(200) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `no_quotation`, `category`, `perusahaan`, `nama`, `jabatan`, `mail`, `date`, `created_by`) VALUES
(7, 'MP-7/QUO/III/2019', 'Broadband', 'PT Indonesia Sukses', 'Agus', 'Manager', 'agus@permana.net.id', '2019-03-05 10:57:16', 10),
(8, 'MP-8/QUO/III/2019', 'Broadband', 'PT. Sumber Daya Makmur', 'Erick Santana P', 'AM', 'erick@gmail.com', '2019-03-12 09:54:48', 2),
(10, 'MP-10/QUO/III/2019', 'Dedicated', 'PT. ABC', 'ERICK', 'Sales', 'erick.santana@permana.net.id', '2019-03-12 19:27:00', 13),
(11, 'MP-11/QUO/III/2019', 'Hospitality', 'PT. ABCD', 'ADE', 'Sales', 'ratih@permana.net.id', '2019-03-12 19:29:48', 13),
(12, 'MP-12/QUO/III/2019', 'Broadband', 'PT. ABC', 'ADE', 'Sales', 'ratih@permana.net.id', '2019-03-12 19:44:40', 8),
(13, 'MP-13/QUO/III/2019', 'Hospitality', 'PT Cloud Data', 'Budi Merdeka', 'Manager', 'ags@permana.net.id', '2019-03-27 11:48:12', 2);

-- --------------------------------------------------------

--
-- Table structure for table `syarat_ketentuan`
--

CREATE TABLE `syarat_ketentuan` (
  `id_quotation` int(11) NOT NULL,
  `isi_sk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `syarat_ketentuan`
--

INSERT INTO `syarat_ketentuan` (`id_quotation`, `isi_sk`) VALUES
(7, 'Penawaran berlaku 14  hari sejak tanggal penawaran'),
(7, 'SLA 99.8%'),
(7, 'Free Router Mikrotik 1 unit ( maksimal RB750)'),
(7, 'Helpdesk 7 X 24 jam'),
(7, 'Lama Pengerjaan - Fiber Optik 4 s/d 5 minggu setelah terima PO'),
(7, 'Support QinQ dan Multi Vlan');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(15) NOT NULL,
  `position` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `password`, `level`, `position`) VALUES
(2, 'Administrator', 'admin@example.com', 'e10adc3949ba59abbe56e057f20f883e', 'Admin', 'Super Admin'),
(17, 'Sales', 'sales@example.com', 'e10adc3949ba59abbe56e057f20f883e', 'Sales', 'Account Manager'),
(18, 'Purchasing', 'purchasing', '202cb962ac59075b964b07152d234b70', 'Purchasing', 'Purchasing'),
(20, 'Manager', 'manager@example.com', 'e10adc3949ba59abbe56e057f20f883e', 'Manager', 'Manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `penawaran`
--
ALTER TABLE `penawaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_purchase_order` (`no_purchase_order`(11));

--
-- Indexes for table `purchase_order_item`
--
ALTER TABLE `purchase_order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_quotation` (`no_quotation`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `penawaran`
--
ALTER TABLE `penawaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `purchase_order_item`
--
ALTER TABLE `purchase_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
