<?php if($action == "Create") : ?>
<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title mt-0">Add Item Quotation</h5>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	</div>
	<form method="POST" action="<?php echo site_url('Quotation/quotationDetailCreateProcess') ?>">
		<div class="modal-body">
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Produk/Layanan</label>
				<div class="col-sm">
					<input name="produk" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
					<input name="id_quotation" type="hidden" class="form-control" value="<?php echo $id_quotation ?>" required
					 autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Segmen</label>
				<div class="col-sm">
					<input name="segmen" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Kapasitas</label>
				<div class="col-sm">
					<input name="kapasitas" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Media Akses</label>
				<div class="col-sm">
					<input name="media_akses" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Kontrak</label>
				<div class="col-sm">
					<input name="kontrak" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Alamat Instalasi</label>
				<div class="col-sm">
					<input name="alamat_instalasi" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Biaya Instalasi</label>
				<div class="col-sm">
					<input name="biaya_instalasi" type="number" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Biaya Bulanan</label>
				<div class="col-sm">
					<input name="biaya_bulanan" type="number" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<div class="form-group row">
				<div class="col-sm">
					<button type="submit" class="btn btn-success pull-right"><i class="fa fa-print"></i> Save</button>
				</div>
				<div class="col-sm">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</form>
</div>

	<script type="text/javascript">
    $(document).ready(function(){

    // Format mata uang.
    $( '.uang' ).mask('000.000.000', {reverse: true});
		})
	</script>

<?php endif; ?>
