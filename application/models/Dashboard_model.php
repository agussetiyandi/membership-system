<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public function get_sum()
	{
		$sql = "SELECT sum(id) as total FROM quotation";
		$result = $this->db->query($sql);
		return $result->row()->id;
	}

}
