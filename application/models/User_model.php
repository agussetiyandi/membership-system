<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all_data_user()
	{
		$this->db->select("*");
		$this->db->from("user");

		return $this->db->get();
	}

	function get_data_user($data)
	{
		$this->db->select("*");
		$this->db->from("user");
		$this->db->where("id", $data);

		return $this->db->get();
	}

	function userCreateProcessDB($data){
    $this->db->insert("user", $data);
	}

	function userUpdateProcessDB($data, $condition){
		$this->db->where($condition);
		$this->db->update("user", $data);
	}

	function userDeleteProsesDB($data){
		$this->db->where("id", $data);
		$this->db->delete("user");
	}

}
