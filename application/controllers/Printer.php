<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printer extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('Pdf');
		$this->load->model("Printer_model");
		$this->load->model("Quotation_model");
	}

	public function index(){
		$data['penawaran'] = $this->Printer_model->get_all_data_penawaran();
		$this->load->view('Dashboard', $data);
	}

	function printquotation($id){
		$id_user = $this->session->userdata("id");
		$id_user = 2;
		$data['penawaran'] = $this->Printer_model->get_data_penawaran($id);
		$data['quotation'] = $this->Quotation_model->get_data_quotation($id);
		$data['syarat_ketentuan'] = $this->Quotation_model->get_syarat_ketentuan($id);
		$data['data'] = array(
			'u_level' => $this->session->userdata("level"),
			'u_position' => $this->session->userdata("position"),
			'u_nama' => $this->session->userdata("nama")
		);
		$draft = $this->Printer_model->get_draft($id);
		$this->load->view('print_'.$draft, $data);
	}

	// PURCHASE ORDER ITEM
	function printpurchaseorder($id){
		$id_user = $this->session->userdata("id");
		$id_user = 2;
		$data['purchase_order_item'] = $this->Printer_model->get_data_purchase_order_item($id);
		$data['purchase_order_item'] = $this->PO_model->get_data_purchase_order($id);
		$data['data'] = array(
			'u_level' => $this->session->userdata("level"),
			'u_position' => $this->session->userdata("position"),
			'u_nama' => $this->session->userdata("nama")
		);
		$draft = $this->Printer_model->get_draft($id);
		$this->load->view('print_'.$draft, $data);
	}
}
