<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->library('Pdf');
		$this->load->model("Staff_mdl");
	}

	public function index(){
		$this->load->view('staffCreate');
	}

	public function staffCreate(){
		$this->load->view('staffCreate');
	}

	public function companyCreateProcess(){
		$data = array(
			'company_name' => $this->input->post('company_name'),
			'company_email' => $this->input->post('company_email'),
			'company_phone' => $this->input->post('company_phone'),
			'company_category' => $this->input->post('company_category'),
			'company_qty_employee' => $this->input->post('company_qty_employee'),
			'company_desc' => $this->input->post('company_desc'),
			'company_logo' => $this->input->post('company_logo'),
			'created_by' => $this->session->userdata("id")
		);
	}

	public function staffList(){
		$data['t_staff'] = $this->Staff_mdl->get_all_data_staff();
		$this->load->view('staffList', $data);
	}

	public function staffUpdate($staff_id){
		$data['t_staff'] = $this->Staff_mdl->get_data_staff($staff_id);
		$this->load->view('staffUpdate', $data);
	}

	public function companyUpdateProcess(){
		$data = array(
			'company_name' => $this->input->post('company_name'),
			'company_email' => $this->input->post('company_email'),
			'company_phone' => $this->input->post('company_phone'),
			'company_category' => $this->input->post('company_category'),
			'company_qty_employee' => $this->input->post('company_qty_employee'),
			'company_desc' => $this->input->post('company_desc'),
			'company_logo' => $this->input->post('company_logo'),
		);
		$condition['company_id'] = $this->input->post('company_id');
		$this->Compnay_mdl->companyUpdateProcessDB($data, $condition);
		redirect('Company/companyList');
	}

	public function companyDeleteProses($company_id){
		$this->Company_mdl->companyDeleteProsesDB($company_id);
		redirect('Company/companyList');
	}

	//Detail
	public function quotationDetail($id){
		$data['penawaran'] = $this->Quotation_model->get_all_data_quotation_detail($id);
		$data['syarat_ketentuan'] = $this->Quotation_model->get_syarat_ketentuan($id)->num_rows();
		$data['id_quotation'] = $id;
		$this->load->view('quotationDetail', $data);
	}

	public function quotationDetailCreate($id_quotation){
		$data['action'] = "Create";
		$data['id_quotation'] = $id_quotation;
		$this->load->view('modal', $data);
	}

	public function quotationDetailCreateProcess(){
		$data = array(
			'id_quotation' => $this->input->post('id_quotation'),
			'produk' => $this->input->post('produk'),
			'segmen' => $this->input->post('segmen'),
			'kapasitas' => $this->input->post('kapasitas'),
			'media_akses' => $this->input->post('media_akses'),
			'kontrak' => $this->input->post('kontrak'),
			'alamat_instalasi' => $this->input->post('alamat_instalasi'),
			'biaya_instalasi' => $this->input->post('biaya_instalasi'),
			'biaya_bulanan' => $this->input->post('biaya_bulanan')
		);
		$id_quotation = $this->input->post('id_quotation');
		$this->Quotation_model->quotationDetailCreateProcessDB($data);
		redirect('Quotation/quotationDetail/'.$id_quotation);
	}

	public function quotationDetailDeleteProses($id, $id_quotation){
		$this->Quotation_model->quotationDetailDeleteProsesDB($id);
		redirect('Quotation/quotationDetail/'.$id_quotation);
	}

	public function quotationSKCreate($id_quotation){
		$data['action'] = "Create";
		$data['id_quotation'] = $id_quotation;
		$this->load->view('modalSK', $data);
	}

	public function quotationSKCreateProcess(){
		$post=$this->input->post();
		$isi_sk = (isset($post->sk)?array_keys($post->sk):array());
		$data = array(
			'id_quotation' => $this->input->post('id_quotation'),
			'isi_sk' => $this->input->post('sk')
		);
		$id_quotation = $this->input->post('id_quotation');
		$this->Quotation_model->quotationSKCreateProcessDB($data);
		redirect('Quotation/quotationDetail/'.$id_quotation);
	}
}
