<?php if($action == "Create") : ?>
<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title mt-0">Syarat & Ketentuan</h5>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	</div>
	<form method="POST" action="<?php echo site_url('Quotation/quotationSKCreateProcess') ?>">
		<div class="modal-body">
			<input name="id_quotation" type="hidden" class="form-control" value="<?php echo $id_quotation ?>" required
				autocomplete="off">

			<h6><strong>BROADBAND</strong></h6>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Penawaran berlaku 14 hari sejak tanggal penawaran" name="sk[]"> Penawaran
					berlaku 14 hari sejak tanggal penawaran
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="SLA 99.5%" name="sk[]"> SLA 99.5%
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Free Router Mikrotik RB941" name="sk[]"> Free Router Mikrotik RB941
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Helpdesk 7 X 24 jam" name="sk[]"> Helpdesk 7 X 24 jam
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox"
						value="Pemutusan kontrak sebelum waktu kontrak selesai secara sepihak oleh pelanggan akan dikenakan denda sebesar 50% x biaya bulanan x sisa masa berlangganan"
						name="sk[]"> Pemutusan kontrak sebelum waktu kontrak selesai secara sepihak oleh pelanggan akan dikenakan
					denda sebesar 50% x biaya bulanan x sisa masa berlangganan
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox"
						value="Berlanggan tidak sampai 1 tahun, pembayaran dilakukan diawal (biaya instalasi dan berlangganan paket selama periode tertentu)"
						name="sk[]"> Berlanggan tidak sampai 1 tahun, pembayaran dilakukan diawal (biaya instalasi dan berlangganan
					paket selama periode tertentu)
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Lama Pengerjaan – Wireless 7 hari maksimal setelah terima PO" name="sk[]"> Lama
					Pengerjaan – Wireless 7 hari maksimal setelah terima PO
				</label>
			</div>

			<h6><strong>DEDICATED</strong></h6>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Penawaran berlaku 14 hari sejak tanggal penawaran" name="sk[]"> Penawaran
					berlaku 14 hari sejak tanggal penawaran
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Monitoring Trafik Internet berbasis Website (MRTG)" name="sk[]"> Monitoring
					Trafik Internet berbasis Website (MRTG)
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="IP Publik /30 maksimal 4 IP" name="sk[]"> IP Publik /30 maksimal 4 IP
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Free Router Mikrotik 1 unit (Maksimal RB750)" name="sk[]"> Free Router Mikrotik
					1 unit (Maksimal RB750)
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="SLA 99.9%" name="sk[]"> SLA 99.9%
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Helpdesk 7 X 24 jam" name="sk[]"> Helpdesk 7 X 24 jam
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Free Intalasi jika Kontrak Minimal 2 Tahun" name="sk[]"> Free Intalasi jika
					Kontrak Minimal 2 Tahun
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox"
						value="Pemutusan kontrak sebelum waktu kontrak selesai secara sepihak oleh pelanggan akan dikenakan denda sebesar 50% x biaya bulanan x sisa masa berlangganan."
						name="sk[]"> Pemutusan kontrak sebelum waktu kontrak selesai secara sepihak oleh pelanggan akan dikenakan
					denda sebesar 50% x biaya bulanan x sisa masa berlangganan.
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox"
						value="Berlanggan tidak sampai 1 tahun, pembayaran dilakukan diawal (biaya instalasi dan berlangganan paket selama periode tertentu)"
						name="sk[]"> Berlanggan tidak sampai 1 tahun, pembayaran dilakukan diawal (biaya instalasi dan berlangganan
					paket selama periode tertentu)
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Lama Pengerjaan – Wireless 7 hari maksimal setelah terima PO" name="sk[]"> Lama
					Pengerjaan – Wireless 7 hari maksimal setelah terima PO
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Lama Pengerjaan – Fiber Optik 4 s/d 5 minggu setelah terima PO" name="sk[]">
					Lama Pengerjaan – Fiber Optik 4 s/d 5 minggu setelah terima PO
				</label>
			</div>

			<h6><strong>HOSPITALITY</strong></h6>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Penawaran berlaku 14 hari sejak tanggal penawaran" name="sk[]"> Penawaran
					berlaku 14 hari sejak tanggal penawaran
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Monitoring Trafik Internet berbasis Website (MRTG)" name="sk[]"> Monitoring
					Trafik Internet berbasis Website (MRTG)
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Sewa AP (Access Point) Rp. 100.000 (Per 1 AP)/ Bulan" name="sk[]"> Sewa AP
					(Access Point) Rp. 100.000 (Per 1 AP)/ Bulan
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="IP Publik sesuai permintaan maksimal 8 IP" name="sk[]"> IP Publik sesuai
					permintaan maksimal 8 IP
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Free Router Mikrotik 1 Unit (Maksimal RB750)" name="sk[]"> Free Router Mikrotik
					1 Unit (Maksimal RB750)
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="SLA 99.9%" name="sk[]"> SLA 99.9%
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Helpdesk 7 X 24 jam" name="sk[]"> Helpdesk 7 X 24 jam
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Free Intalasi jika Kontrak Minimal 2 Tahun" name="sk[]"> Free Intalasi jika
					Kontrak Minimal 2 Tahun
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox"
						value="Pemutusan kontrak sebelum waktu kontrak selesai secara sepihak oleh pelanggan akan dikenakan denda sebesar 50% x biaya bulanan x sisa masa berlangganan."
						name="sk[]"> Pemutusan kontrak sebelum waktu kontrak selesai secara sepihak oleh pelanggan akan dikenakan
					denda sebesar 50% x biaya bulanan x sisa masa berlangganan.
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox"
						value="Berlanggan tidak sampai 1 tahun, pembayaran dilakukan diawal (biaya instalasi dan berlangganan paket selama periode tertentu)"
						name="sk[]"> Berlanggan tidak sampai 1 tahun, pembayaran dilakukan diawal (biaya instalasi dan berlangganan
					paket selama periode tertentu)
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Lama Pengerjaan – Wireless 7 hari maksimal setelah terima PO" name="sk[]"> Lama
					Pengerjaan – Wireless 7 hari maksimal setelah terima PO
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Lama Pengerjaan – Fiber Optik 4 s/d 5 minggu setelah terima PO" name="sk[]">
					Lama Pengerjaan – Fiber Optik 4 s/d 5 minggu setelah terima PO
				</label>
			</div>

			<h6><strong>SOLUTION</strong></h6>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Penawaran berlaku 14 hari sejak tanggal penawaran" name="sk[]"> Penawaran
					berlaku 14 hari sejak tanggal penawaran
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Free Router Mikrotik 1 unit (Maksimal RB750)" name="sk[]"> Free Router Mikrotik
					1 unit (Maksimal RB750)
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="SLA 99.8%" name="sk[]"> SLA 99.8%
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Helpdesk 7 X 24 jam" name="sk[]"> Helpdesk 7 X 24 jam
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Lama Pengerjaan – Fiber Optik 4 s/d 5 minggu setelah terima PO" name="sk[]">
					Lama Pengerjaan – Fiber Optik 4 s/d 5 minggu setelah terima PO
				</label>
			</div>
			<div class="form-group row">
				<label class="col-sm">
					<input type="checkbox" value="Support QinQ dan Multi Vlan" name="sk[]"> Support QinQ dan Multi Vlan
				</label>
			</div>
		</div>
		<div class="modal-footer">
			<div class="form-group row">
				<div class="col-sm">
					<button type="submit" class="btn btn-success pull-right"><i class="fa fa-print"></i> Save</button>
				</div>
				<div class="col-sm">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</form>
</div>

<?php endif; ?>
