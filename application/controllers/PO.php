<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PO extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('Pdf');
		$this->load->model("PO_model"); 
	}

	public function index(){
		$this->load->view('purchaseOrderCreate');
	}

	public function purchaseOrderCreate(){
		$this->load->view('purchaseOrderCreate');
	}

	public function purchaseOrderCreateProcess(){
		$data = array(
			// 'po_date' => $this->input->post('po_date'),
			'nama_perusahaan' => $this->input->post('nama_perusahaan'),
			'nama_pic' => $this->input->post('nama_pic'),
			'alamat' => $this->input->post('alamat'),
			'note' => $this->input->post('note'),
			'email' => $this->input->post('email')
		);
		//int to roman
		$lookup = array(
			'X' => 10,
			'IX' => 9,
			'V' => 5,
			'IV' => 4,
			'I' => 1);
		$integer = date("n");
		$integer = intval($integer);
 		$result = '';
 		foreach($lookup as $roman => $value){
		  $matches = intval($integer/$value);
		  $result .= str_repeat($roman,$matches);
		  $integer = $integer % $value;
		}
		//end int to roman
		$id_purchase_order = '/PO/'.$result.'/'.date("Y");
		$date = date("Y-m-d H:i:s");
		$this->PO_model->purchaseOrderCreateProcessDB($data, $id_purchase_order, $date);
		redirect('PO/purchaseOrderList');
	}
	
	public function purchaseOrderList(){
		$data['purchase_order'] = $this->PO_model->get_all_data_purchase_order();
		$this->load->view('purchaseOrderList', $data);
	}

	public function purchaseOrderUpdate($id){
		$data['purchase_order'] = $this->PO_model->get_data_purchase_order($id);
		$this->load->view('purcaseOrderUpdate', $data);
	}

	public function purchaseOrderUpdateProcess(){
		$data = array(
			// 'po_date' => $this->input->post('po_date'),
			'nama_perusahaan' => $this->input->post('nama_perusahaan'),
			'nama_pic' => $this->input->post('nama_pic'),
			'alamat' => $this->input->post('alamat'),
			'note' => $this->input->post('note'),
			'email' => $this->input->post('email')
		);
		$condition['id'] = $this->input->post('id');
		$this->PO_model->purchaseOrderUpdateProcessDB($data, $condition);
		redirect('PO/purchaseOrderList');
	}

	public function purchaseOrderDeleteProses($id){
		$this->PO_model->purchaseOrderDeleteProsesDB($id);
		redirect('PO/purchaseOrderList');
	}

	//Detail
	public function purchaseOrderDetail($id){
		$data['purchase_order_item'] = $this->PO_model->get_all_data_purchase_order_detail($id);
		$data['id_purchase_order'] = $id;
		$this->load->view('purchaseOrderDetail', $data);
	}

	public function purchaseOrderDetailCreate($id_purchase_order){
		$data['action'] = "Create";
		$data['id_purchase_order'] = $id_purchase_order;
		$this->load->view('modalPurchaseOrder', $data);
	}

	public function purchaseOrderDetailCreateProcess(){
		$data = array(
			'id_purchase_order' => $this->input->post('id_purchase_order'),
			'produk' => $this->input->post('produk'),
			'media_akses' => $this->input->post('media_akses'),
			'alamat_instalasi' => $this->input->post('alamat_instalasi'),
			'alamat_backhaule' => $this->input->post('alamat_backhaule'),
			'kapasitas' => $this->input->post('kapasitas'),
			'harga_satuan' => $this->input->post('harga_satuan'),
			'total' => $this->input->post('total')
		);
		$id_purchase_order = $this->input->post('id_purchase_order');
		$this->PO_model->purchaseOrderDetailCreateProcessDB($data);
		redirect('PO/purchaseOrderDetail/'.$id_purchase_order);
	}

	public function purchaseOrderDetailDeleteProses($id, $id_purchase_order){
		$this->PO_model->purchaseOrderDetailDeleteProsesDB($id);
		redirect('PO/purchaseOrderDetail/'.$id_purchase_order);
	}
}
