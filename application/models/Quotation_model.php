<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all_data_quotation()
	{
		$this->db->select("*");
		$this->db->from("quotation");
		if($this->session->userdata("level") == "Sales"){
			$this->db->where("created_by", $this->session->userdata("id"));
		}

		return $this->db->get();
	}

	function get_data_quotation($data)
	{
		$this->db->select("*");
		$this->db->from("quotation");
		$this->db->where("id", $data);

		return $this->db->get();
	}

	function quotationCreateProcessDB($data, $id_quotation, $date){
		$this->db->insert("quotation", $data);
		$sql = "UPDATE quotation SET no_quotation = CONCAT('MP-', `id`, '".$id_quotation."'), date = '".$date."' WHERE date IS NULL";
		$this->db->query($sql);
	}

	function quotationUpdateProcessDB($data, $condition){
		$this->db->where($condition);
		$this->db->update("quotation", $data);
	}

	function quotationDeleteProsesDB($data){
		$this->db->where("id", $data);
		$this->db->delete("quotation");
	}

	//Detail
	function get_all_data_quotation_detail($id)
	{
		$this->db->select("*");
		$this->db->from("penawaran");
		$this->db->where("id_quotation", $id);

		return $this->db->get();
	}

	function get_syarat_ketentuan($id)
	{
		$this->db->select("*");
		$this->db->from("syarat_ketentuan");
		$this->db->where("id_quotation", $id);

		return $this->db->get();
	}

	function quotationDetailCreateProcessDB($data){
		$this->db->insert("penawaran", $data);
	}

	function quotationDetailDeleteProsesDB($data){
		$this->db->where("id", $data);
		$this->db->delete("penawaran");
	}

	function quotationSKCreateProcessDB($data){
		foreach($data['isi_sk'] as $isi_sk){
			$this->db->insert("syarat_ketentuan", array('id_quotation'=>$data['id_quotation'],'isi_sk'=>$isi_sk));
		}
	}

}
