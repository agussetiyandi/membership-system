<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PO_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all_data_purchase_order()
	{
		$this->db->select("*");
		$this->db->from("purchase_order");

		return $this->db->get();
	}

	function get_data_purchase_order($data)
	{
		$this->db->select("*");
		$this->db->from("purchase_order");
		$this->db->where("id", $data);
		
		return $this->db->get();
	}

	function purchaseOrderCreateProcessDB($data, $id_purchase_order, $date){
		$this->db->insert("purchase_order", $data);
		$sql = "UPDATE purchase_order SET no_purchase_order = CONCAT('PO-', `id`, '".$id_purchase_order."'), date = '".$date."' WHERE date IS NULL";
		$this->db->query($sql);
	}

	function purchaseOrderUpdateProcessDB($data, $condition){
		$this->db->where($condition);
		$this->db->update("purchase_order", $data);
	}

	function purchaseOrderDeleteProsesDB($data){
		$this->db->where("id", $data);
		$this->db->delete("purchase_order");
	}

	//Detail
	function get_all_data_purchase_order_detail($id)
	{
		$this->db->select("*");
		$this->db->from("purchase_order_item");
		$this->db->where("id_purchase_order", $id);

		return $this->db->get();
	}

	function purchaseOrderDetailCreateProcessDB($data){
		$this->db->insert("purchase_order_item", $data);
	}

	function purchaseOrderDetailDeleteProsesDB($data){
		$this->db->where("id", $data);
		$this->db->delete("purchase_order_item");
	}

}
