<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Add Quotation</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Add Quotation</h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">Quotation</a></li>
										<li class="breadcrumb-item active" aria-current="page">Add Quotation</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-xl-12">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-6">
										<div class="p-20">
                      <?php echo form_open_multipart('Quotation/addQuotation'); ?>
											<div class="form-group"><label>Quotation Category</label>
												<select name="category" class="form-control">
													<?php foreach($list_cat->result() as $row) : ?>
													<option value="<?= $row->quotation_category ?>">
														<?= $row->quotation_category ?>
													</option>
													<?php endforeach; ?>
												</select>
                      </div>
                    </div>
                  </div>
                </div>  
                <div class="row">
                  <div class="col-md-6">
										<div class="p-20">
											<?php echo form_open_multipart('Quotation/addQuotation'); ?>
											<div class="form-group">
												<label>Nama Perusahaan</label>
												<input name="company_name" type="text" class="form-control" placeholder=". . ." required>
											</div>
											<div class="form-group">
												<label>Email</label>
												<input name="email" type="text" class="form-control" placeholder=". . .">
											</div>
											<div class="form-group">
												<label>Nomor Telephone</label>
												<input name="phone_number" type="text" class="form-control" placeholder=". . .">
											</div>
											<div class="form-group">
												<label>PIC</label>
												<input type="text" name="pic" class="form-control" placeholder=". . .">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="p-20">
											<div class="form-group">
												<label>Produk</label>
												<input type="text" name="produk" class="form-control" placeholder=". . .">
											</div>
											<div class="form-group">
												<label>Media Akses</label>
												<input type="text" name="media_access" class="form-control" placeholder=". . .">
											</div>
											<div class="form-group">
												<label>Kontrak</label>
												<input type="text" name="contract" class="form-control" placeholder=". . .">
											</div>
											<div class="form-group">
												<label>Biaya Instalasi</label>
												<input type="text" name="price_installation" class="form-control" placeholder=". . .">
											</div>
											<div class="form-group">
												<label>Biaya Bulanan</label>
												<input type="text" name="monthly_installation" class="form-control" placeholder=". . .">
											</div>
										</div>
									</div>
									<div class="form-group col-xl-6">
										<button type="submit" name="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
									</div>
								</div>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- end row -->
	</div><!-- container-fluid -->
	</div><!-- content -->
	<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->

	<?php $this->load->view("_partials/js.php") ?>

</body>

</html>
