<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Purchase Order System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">PROFILE</h4>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->

				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h6 class="pull-left">Item Quotation</h6>
								<div class="row">
									<div class="col-lg-12 col-xl-4 col-md-12 col-sm-12">
										<div class="card">
											<div class="card-body">
												<div class="text-center">
													<div class="userprofile social">
														<div class="userpic"> <img src="assets/imgimages/users/avatar-8.jpg" alt=""
																class="userpicimg">
														</div>
														<h3 class="username">Agus Setiyandi</h3>
														<p>Web Designer, USA</p>
														<div class="text-center mb-2">
															<span><i class="fa fa-star text-warning"></i></span>
															<span><i class="fa fa-star text-warning"></i></span>
															<span><i class="fa fa-star text-warning"></i></span>
															<span><i class="fa fa-star-half-o text-warning"></i></span>
															<span><i class="fa fa-star-o text-warning"></i></span>
														</div>
														<div class="socials text-center"> <a href="#" class="btn btn-circle btn-primary ">
																<i class="fa fa-facebook"></i></a> <a href="#" class="btn btn-circle btn-danger ">
																<i class="fa fa-google-plus"></i></a> <a href="#" class="btn btn-circle btn-info ">
																<i class="fa fa-twitter"></i></a> <a href="#" class="btn btn-circle btn-warning "><i
																	class="fa fa-envelope"></i></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>








					</div><!-- container-fluid -->
				</div><!-- content -->
				<?php $this->load->view("_partials/footer.php") ?>
			</div><!-- ============================================================== -->
			<!-- End Right content here -->
			<!-- ============================================================== -->
		</div><!-- END wrapper -->
		<!-- jQuery  -->
		<div id="modalCE" class="modal fade">
			<div class="modal-dialog" id="modalContent">

			</div><!-- /.modal-dialog -->
		</div>

		<?php $this->load->view("_partials/js.php") ?>

		<script type="text/javascript">
			function modalCreate() {
				$.ajax({
					url: "<?php echo site_url('Printer/modalCreate') ?>",
					success: function (data) {
						$("#modalContent").html(data);
					}
				});
				$("#modalCE").modal("show");
			}
		</script>
</body>

</html>
