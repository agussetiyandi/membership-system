<?php if($action == "Create") : ?>
<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title mt-0">Add Item Purchase Order</h5>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	</div>
	<form method="POST" action="<?php echo site_url('PO/purchaseOrderDetailCreateProcess') ?>">
		<div class="modal-body">
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Produk/Layanan</label>
				<div class="col-sm">
					<input name="produk" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
					<input name="id_purchase_order" type="hidden" class="form-control" value="<?php echo $id_purchase_order ?>" required
					 autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Media Akses</label>
				<div class="col-sm">
					<input name="media_akses" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Alamat Instalasi</label>
				<div class="col-sm">
					<input name="alamat_instalasi" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Alamat Backhaule</label>
				<div class="col-sm">
					<input name="alamat_backhaule" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Kapasitas</label>
				<div class="col-sm">
					<input name="kapasitas" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Harga Satuan</label>
				<div class="col-sm">
					<input name="harga_satuan" type="text" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 col-form-label">Total</label>
				<div class="col-sm">
					<input name="total" type="number" class="form-control" placeholder=". . ." required autocomplete="off">
				</div>
			</div>

		</div>
		<div class="modal-footer">
			<div class="form-group row">
				<div class="col-sm">
					<button type="submit" class="btn btn-success pull-right"><i class="fa fa-print"></i> Save</button>
				</div>
				<div class="col-sm">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</form>
</div>

<?php endif; ?>
