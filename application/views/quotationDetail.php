<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Purchase Order System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Quotation</h4>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h6 class="pull-left">Item Quotation</h6>
								<a href="<?php echo site_url('Printer/printquotation/'.$id_quotation) ?>" target="_blank" class="btn btn-success pull-right mx-2">
									<i class="fa fa-print"> Print</i>
								</a>
								<?php if($syarat_ketentuan==0){ ?>
								<button onClick="modalCreateSK();" type="button" class="btn btn-success pull-right mx-1">
									<i class="fa fa-info"> Syarat & Ketentuan</i>
								</button>
								<?php } ?>
								<button onClick="modalCreate();" type="button" class="btn btn-success pull-right">
									<i class="fa fa-plus"> Add Item</i>
								</button>
							</div>
							<div class="card-body">
								<table id="datatable" class="table table-bordered dt-responsive text-center" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="1%">No</th>
											<th>Produk/Layanan</th>
											<th>Segmen</th>
											<th>Kapasitas</th>
											<th>Media Akses</th>
											<th>Kontrak</th>
											<th>Alamat Instalasi</th>
											<th>Biaya Instalasi</th>
											<th>Biaya Bulanan</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach($penawaran->result() as $row) : ?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?= $row->produk; ?></td>
											<td><?= $row->segmen; ?></td>
											<td><?= $row->kapasitas; ?></td>
											<td><?= $row->media_akses; ?></td>
											<td><?= $row->kontrak; ?></td>
											<td><?= $row->alamat_instalasi; ?></td>
											<td>Rp<?= number_format($row->biaya_instalasi) ?></td>
											<td>Rp<?= number_format($row->biaya_bulanan) ?></td>
											<td>
												<a href="<?php echo site_url('Quotation/quotationDetailDeleteProses/'.$row->id.'/'.$id_quotation) ?>" onclick="return confirm('Are you sure?')" title="Delete" type="button" class="btn btn-danger">
													<i class="fa fa-times"></i>
												</a>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->
	<div id="modalCE" class="modal fade">
		<div class="modal-dialog" id="modalContent">

		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>

	<script type="text/javascript">
		$("#datatable").DataTable();
		function modalCreate() {
			$.ajax({
					url: "<?php echo site_url('Quotation/quotationDetailCreate/'.$id_quotation) ?>",
					success: function(data){
						$("#modalContent").html(data);
					}
				});
			$("#modalCE").modal("show");
		}
		function modalCreateSK() {
			$.ajax({
					url: "<?php echo site_url('Quotation/quotationSKCreate/'.$id_quotation) ?>",
					success: function(data){
						$("#modalContent").html(data);
					}
				});
			$("#modalCE").modal("show");
		}
	</script>
</body>

</html>
