<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Membership System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Edit Company</h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">Company</a></li>
										<li class="breadcrumb-item active" aria-current="page">Edit Data Company</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-xl-12">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-6">
										<div class="p-20">
											<?php foreach($t_company->result() as $row) : ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="p-20">
											<form method="POST" action="<?php echo site_url('Company/companyUpdateProcess') ?>">
												<div class="form-group">
													<label>Company Name</label>
													<input name="company_name" type="text" class="form-control" placeholder=". . ."
														value="<?= $row->company_name ?>" required>
												</div>
												<div class="form-group">
													<label>Email</label>
													<input name="company_email" type="text" class="form-control" placeholder=". . ."
													value="<?= $row->company_email ?>">
												</div>
												<div class="form-group">
													<label>Phone</label>
													<input name="company_phone" type="text" class="form-control" placeholder=". . ."
													value="<?= $row->company_phone ?>">
												</div>
												<div class="form-group">
													<label>Category</label>
													<input type="text" name="company_category" class="form-control" placeholder=". . ."
													value="<?= $row->company_category ?>">
												</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="p-20">
											<div class="form-group">
												<label>Number of Employee</label>
												<input type="text" name="company_qty_employee" class="form-control" placeholder=". . ."
												value="<?= $row->company_qty_employee ?>">
											</div>
											<div class="form-group">
												<label>Company Desc</label>
												<input type="text" name="company_desc" class="form-control" placeholder=". . ."
												value="<?= $row->company_desc ?>">
											</div>
											<div class="form-group">
												<label>PIC</label>
												<input type="text" name="company_pic" class="form-control" placeholder=". . ."
												value="<?= $row->company_pic ?>">
											</div>
											<div class="form-group">
												<label>Company Logo</label>
												<input type="file" name="picture" class="filestyle" id="picture"
													data-buttonname="btn-secondary">
													
											</div>
										</div>
									</div>

									<div class="form-group col-xl-6">
										<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>  Save</button>
										
									</div>


									<!-- <div class="form-group row">
										<label class="col-sm-2"></label>
										<div class="col-sm">
											<button type="submit" class="btn btn-success pull-right" style="margin: 0px 5px;"><i
													class="fa fa-save"></i> Save</button>
											<a href="<?php echo site_url('Company/companyList') ?>" class="btn btn-danger pull-right"
												style="margin: 0px 5px;"><i class="fa fa-times"></i>
												Cancel</a>
										</div>
									</div> -->
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- end row -->
			<?php endforeach; ?>
		</div><!-- container-fluid -->
	</div><!-- content -->
	<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->
	<div id="modalCE" class="modal fade">
		<div class="modal-dialog" id="modalContent">

		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>

	<script type="text/javascript">
		function modalCreate() {
			$.ajax({
				url: "<?php echo site_url('Printer/modalCreate') ?>",
				success: function (data) {
					$("#modalContent").html(data);
				}
			});
			$("#modalCE").modal("show");
		}

	</script>
</body>

</html>
