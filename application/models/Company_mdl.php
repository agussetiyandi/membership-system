<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_mdl extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all_data_company()
	{
		$this->db->select("*");
		$this->db->from("t_company");

		return $this->db->get();
	}

	function get_data_company($data)
	{
		$this->db->select("*");
		$this->db->from("t_company");
		$this->db->where("company_id", $data);

		return $this->db->get();
	}

	function companyCreateProcessDB($data){
    $this->db->insert("t_company", $data);
	}

	function companyUpdateProcessDB($data, $condition){
		$this->db->where($condition);
		$this->db->update("t_company", $data);
	}

	function companyDeleteProsesDB($data){
		$this->db->where("company_id", $data);
		$this->db->delete("t_company");
	}

}
