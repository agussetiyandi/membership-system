<?php

class Login extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('M_login');

	}

	function index(){
		$this->load->view('login');
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$cek = $this->M_login->cek_login($username,$password)->num_rows();
		if($cek > 0){
			$data = $this->M_login->cek_login($username, $password)->row_array();
			$nama = $data['name'];
			$level = $data['level'];
			$id = $data['id'];
			$position = $data['position'];

			$data_session = array(
				'id' => $id,
				'nama' => $nama,
				'level' => $level,
				'position' => $position,
				'status' => 'login'
			);

			$this->session->set_userdata($data_session);

			redirect('Dashboard');

		}else{
			echo $this->session->set_flashdata('msg','Username or Password is Wrong');
			redirect(base_url('login'));
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}
