<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Purchase Order System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Data Company</h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">Company</a></li>
										<li class="breadcrumb-item active" aria-current="page">Data Company</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<!-- <div class="card-header">
								<h6 class="pull-left">Data Company</h6>
							</div> -->
							<div class="card-body">
								<table id="datatable" class="table table-bordered dt-responsive text-center"
									style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="1%">No</th>
											<th>Logo</th>
											<th width="20%">Company Name</th>
											<th width="10%">Email</th>
											<th>Phone</th>
											<th>Category</th>
											<th>Qty Employee</th>
											<th>PIC</th>
											<th width="5%"></th>
										</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach($t_company->result() as $row) : ?>
										<tr>
											<td><?= $no++; ?></td>
											<td>
											<img src="<?= site_url('assets/images/company/'.$row->company_logo) ?>" width="100px">
											</td>
											<td><?= $row->company_name; ?></td>
											<td><?= $row->company_email; ?></td>
											<td><?= $row->company_phone; ?></td>
											<td><?= $row->company_category; ?></td>
											<td><?= $row->company_qty_employee; ?></td>
											<td><?= $row->company_pic; ?></td>

											<td>
												<div class="btn-group" style="color:white; border-radius: 5px; overflow: hidden;">
													<!-- <a href="<?php echo site_url('Company/companyDetail/'.$row->company_id) ?>" title="Add Item Quotation" type="button"
													 class="btn btn-info">
														<i class="fa fa-plus"></i>
													</a> -->
													<a href="<?php echo site_url('Company/companyUpdate/'.$row->company_id) ?>" title="Edit"
														type="button" class="btn btn-warning">
														<i class="fa fa-pencil"></i>
													</a>
													<?php if($this->session->userdata("level") == "Admin" ): ?>
													<a href="<?php echo site_url('Company/companyDeleteProses/'.$row->company_id) ?>"
														onclick="return confirm('Are you sure?')" title="Delete" type="button"
														class="btn btn-danger">
														<i class="fa fa-times"></i>
													</a>
													<?php endif; ?>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->
	<div id="modalCE" class="modal fade">
		<div class="modal-dialog" id="modalContent">

		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>

	<script type="text/javascript">
		$("#datatable").DataTable();

		function modalCreate() {
			$.ajax({
				url: "<?php echo site_url('Printer/modalCreate') ?>",
				success: function (data) {
					$("#modalContent").html(data);
				}
			});
			$("#modalCE").modal("show");
		}

	</script>
</body>

</html>
