<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Printer</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Print Invoice</h4>
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item active"><a href="#">Print Invoice</a></li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="card">
					<div class="card-body">
						<?php if($this->session->flashdata('msg') != NULL){?>
						<div class="alert alert-danger">
						  <?php echo $this->session->flashdata('msg');?>
						</div>
						<?php } ?>
						<div class="row text-center">
							<div class="col-sm-4 offset-sm-4">
								<form method="post" action="<?php echo site_url('Printer/printBon') ?>">
									<div class="form-group">
										<label class="label-control">Print By PO ID</label>
										<input type="text" class="form-control text-center" name="po_id" required />
									</div>
									<div class="form-group">
										<button type="submit" name="submit" class="btn btn-lg btn-success"><i class="fa fa-print"></i> Print</button>
									</div>
								</form>
							</div>
						</div><!-- end row -->
					</div>
				</div>
			</div>
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->

	<?php $this->load->view("_partials/js.php") ?>
</body>

</html>