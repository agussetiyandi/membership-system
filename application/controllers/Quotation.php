<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('Pdf');
		$this->load->model("Quotation_model");
	}

	public function index(){
		$this->load->view('quotationCreate');
	}

	public function quotationCreate(){
		$this->load->view('quotationCreate');
	}

	public function quotationCreateProcess(){
		$data = array(
			'perusahaan' => $this->input->post('perusahaan'),
			'nama' => $this->input->post('nama'),
			'jabatan' => $this->input->post('jabatan'),
			'mail' => $this->input->post('email'),
			'category' => $this->input->post('category'),
			'created_by' => $this->session->userdata("id")
		);
		//int to roman
		$lookup = array(
			'X' => 10,
			'IX' => 9,
			'V' => 5,
			'IV' => 4,
			'I' => 1);
		$integer = date("n");
		$integer = intval($integer);
 		$result = '';
 		foreach($lookup as $roman => $value){
		  $matches = intval($integer/$value);
		  $result .= str_repeat($roman,$matches);
		  $integer = $integer % $value;
		}
		//end int to roman
		$id_quotation = '/QUO/'.$result.'/'.date("Y");
		$date = date("Y-m-d H:i:s");
		$this->Quotation_model->quotationCreateProcessDB($data, $id_quotation, $date);
		redirect('Quotation/quotationList');
	}

	public function quotationList(){
		$data['quotation'] = $this->Quotation_model->get_all_data_quotation();
		$this->load->view('quotationList', $data);
	}

	public function quotationUpdate($id){
		$data['quotation'] = $this->Quotation_model->get_data_quotation($id);
		$this->load->view('quotationUpdate', $data);
	}

	public function quotationUpdateProcess(){
		$data = array(
			'perusahaan' => $this->input->post('perusahaan'),
			'nama' => $this->input->post('nama'),
			'jabatan' => $this->input->post('jabatan'),
			'mail' => $this->input->post('email'),
			'category' => $this->input->post('category')
		);
		$condition['id'] = $this->input->post('id');
		$this->Quotation_model->quotationUpdateProcessDB($data, $condition);
		redirect('Quotation/quotationList');
	}

	public function quotationDeleteProses($id){
		$this->Quotation_model->quotationDeleteProsesDB($id);
		redirect('Quotation/quotationList');
	}

	//Detail
	public function quotationDetail($id){
		$data['penawaran'] = $this->Quotation_model->get_all_data_quotation_detail($id);
		$data['syarat_ketentuan'] = $this->Quotation_model->get_syarat_ketentuan($id)->num_rows();
		$data['id_quotation'] = $id;
		$this->load->view('quotationDetail', $data);
	}

	public function quotationDetailCreate($id_quotation){
		$data['action'] = "Create";
		$data['id_quotation'] = $id_quotation;
		$this->load->view('modal', $data);
	}

	public function quotationDetailCreateProcess(){
		$data = array(
			'id_quotation' => $this->input->post('id_quotation'),
			'produk' => $this->input->post('produk'),
			'segmen' => $this->input->post('segmen'),
			'kapasitas' => $this->input->post('kapasitas'),
			'media_akses' => $this->input->post('media_akses'),
			'kontrak' => $this->input->post('kontrak'),
			'alamat_instalasi' => $this->input->post('alamat_instalasi'),
			'biaya_instalasi' => $this->input->post('biaya_instalasi'),
			'biaya_bulanan' => $this->input->post('biaya_bulanan')
		);
		$id_quotation = $this->input->post('id_quotation');
		$this->Quotation_model->quotationDetailCreateProcessDB($data);
		redirect('Quotation/quotationDetail/'.$id_quotation);
	}

	public function quotationDetailDeleteProses($id, $id_quotation){
		$this->Quotation_model->quotationDetailDeleteProsesDB($id);
		redirect('Quotation/quotationDetail/'.$id_quotation);
	}

	public function quotationSKCreate($id_quotation){
		$data['action'] = "Create";
		$data['id_quotation'] = $id_quotation;
		$this->load->view('modalSK', $data);
	}

	public function quotationSKCreateProcess(){
		$post=$this->input->post();
		$isi_sk = (isset($post->sk)?array_keys($post->sk):array());
		$data = array(
			'id_quotation' => $this->input->post('id_quotation'),
			'isi_sk' => $this->input->post('sk')
		);
		$id_quotation = $this->input->post('id_quotation');
		$this->Quotation_model->quotationSKCreateProcessDB($data);
		redirect('Quotation/quotationDetail/'.$id_quotation);
	}
}
