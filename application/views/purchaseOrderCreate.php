<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Admin Area - Purchase Order System</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="Agus Setiyandi">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App Icons -->

	<?php $this->load->view("_partials/css.php") ?>

</head>

<body class="fixed-left">
	<!-- Loader -->
	<div id="preloader">
		<div id="status">
			<div class="spinner"></div>
		</div>
	</div><!-- Begin page -->
	<?php $this->load->view("_partials/header.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="row align-items-center">
								<div class="col-md-8">
									<h4 class="page-title mb-0">Purchase Order</h4>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h6>Create Purchase Order</h6>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-md-8">
										<form method="POST" action="<?php echo site_url('PO/purchaseOrderCreateProcess') ?>">
											<!-- <div class="form-group row">
												<label class="col-sm-2 col-form-label">Tanggal PO</label>
												<div class="col-sm">
													<input name="po_date" type="date" class="form-control" placeholder=". . ." required>
												</div>
											</div> -->
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Nama Perusahaan</label>
												<div class="col-sm">
													<input name="nama_perusahaan" type="text" class="form-control" placeholder=". . ." required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Nama PIC</label>
												<div class="col-sm">
													<input name="nama_pic" type="text" class="form-control" placeholder=". . ." required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">E-mail</label>
												<div class="col-sm">
													<input name="email" type="mail" class="form-control" placeholder=". . ." required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Alamat</label>
												<div class="col-sm">
													<input name="alamat" type="text" class="form-control" placeholder=". . ." required>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Note</label>
												<div class="col-sm">			
													<textarea required name="note" type="text" class="form-control" rows="5"></textarea>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2"></label>
												<div class="col-sm">
													<button type="submit" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Create</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<!-- jQuery  -->
	<div id="modalCE" class="modal fade">
		<div class="modal-dialog" id="modalContent">

		</div><!-- /.modal-dialog -->
	</div>

	<?php $this->load->view("_partials/js.php") ?>

	<script type="text/javascript">
		function modalCreate() {
			$.ajax({
				url: "<?php echo site_url('Printer/modalCreate') ?>",
				success: function (data) {
					$("#modalContent").html(data);
				}
			});
			$("#modalCE").modal("show");
		}

	</script>
</body>

</html>
